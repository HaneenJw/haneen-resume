			// for viewing more details about cyber job
			// Get the modal
			var cyberJobModal = document.getElementById('cyberJob');

			// Get the button that opens the modal
			var cyberJobBtn = document.getElementById("cyberBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks the button, open the modal 
			cyberJobBtn.onclick = function () {
				cyberJobModal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function () {
				cyberJobModal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function (event) {
				if (event.target == modal) {
					cyberJobModal.style.display = "none";
				}
			}
			
			
		    // for viewing more details about QA engineer job
			// Get the modal
			var qaJobModal = document.getElementById('qaJob');

			// Get the button that opens the modal
			var qaJobBtn = document.getElementById("qaBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[1];

			// When the user clicks the button, open the modal 
			qaJobBtn.onclick = function () {
				qaJobModal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function () {
				qaJobModal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function (event) {
				if (event.target == modal) {
					qaJobModal.style.display = "none";
				}
			}
			
			//for viewing more details about developer job
			// Get the modal
			var devJobModal = document.getElementById('devJob');

			// Get the button that opens the modal
			var devJobBtn = document.getElementById("devBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[2];

			// When the user clicks the button, open the modal 
			devJobBtn.onclick = function () {
				devJobModal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function () {
				devJobModal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function (event) {
				if (event.target == modal) {
					devJobModal.style.display = "none";
				}
			}
			
			// For viewing slideshow skills
			
			var slideIndex = 0;
			showSlides();

			function showSlides() {
				var i;
				var slides = document.getElementsByClassName("mySlides");
				var dots = document.getElementsByClassName("dot");
				for (i = 0; i < slides.length; i++) {
					slides[i].style.display = "none";  
				}
				slideIndex++;
				if (slideIndex > slides.length) {slideIndex = 1}    
				for (i = 0; i < dots.length; i++) {
					dots[i].className = dots[i].className.replace(" active", "");
				}
					slides[slideIndex-1].style.display = "block";  
					dots[slideIndex-1].className += " active";
					setTimeout(showSlides, 2000); // Change image every 2 seconds
				}

			// For print CV
			
			function printFunction(){
				window.print();
			
			}