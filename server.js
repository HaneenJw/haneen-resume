var swig = require ('swig');
var express = require ('express');
var app = express();
app.use(express.static('public'));
var port= 8080;
app.get('/',function(req,res){
	var template = swig.compileFile(__dirname + '/public/haneenResume.html');
	var output = template({});
	res.send(output);
});
app.listen(port);
